{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# HIDDEN\n",
    "\n",
    "from datascience import *\n",
    "import matplotlib\n",
    "matplotlib.use('Agg', warn=False)\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plots\n",
    "plots.style.use('fivethirtyeight')\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Monty Hall Problem ###\n",
    "This [problem](https://en.wikipedia.org/wiki/Monty_Hall_problem) has flummoxed many people over the years, [mathematicians included](https://web.archive.org/web/20140413131827/http://www.decisionsciences.org/DecisionLine/Vol30/30_1/vazs30_1.pdf). Let's see if we can work it out.\n",
    "\n",
    "The setting is derived from a television game show called \"Let's Make a Deal\". Monty Hall hosted this show in the 1960's, and it has since led to a number of spin-offs. An exciting part of the show was that while the contestants had the chance to win great prizes, they might instead end up with \"zonks\" that were less desirable. This is the basis for what is now known as *the Monty Hall problem*.\n",
    "\n",
    "The setting is a game show in which the contestant is faced with three closed doors. Behind one of the doors is a fancy car, and behind each of the other two there is a goat. The contestant doesn't know where the car is, and has to attempt to find it under the following rules.\n",
    "\n",
    "- The contestant makes an initial choice, but that door isn't opened.\n",
    "- At least one of the other two doors must have a goat behind it. Monty opens one of these doors to reveal a goat, displayed in all its glory in [Wikipedia](https://en.wikipedia.org/wiki/Monty_Hall_problem):\n",
    "\n",
    "![Monty Hall goat](images/monty_hall_goat.png)\n",
    "\n",
    "- There are two doors left, one of which was the contestant's original choice. One of the doors has the car behind it, and the other one has a goat. The contestant now gets to choose which of the two doors to open.\n",
    "\n",
    "The contestant has a decision to make. Which door should she choose to open, if she wants the car? Should she stick with her initial choice, or switch to the other door? That is the Monty Hall problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "### The Solution ###\n",
    "\n",
    "In any problem involving chances, the assumptions about randomness are important. It's reasonable to assume that there is a 1/3 chance that the contestant's initial choice is the door that has the car behind it. \n",
    "\n",
    "The solution to the problem is quite straightforward under this assumption, though the straightforward solution doesn't convince everyone. Here it is anyway.\n",
    "\n",
    "- The chance that the car is behind the originally chosen door is 1/3.\n",
    "- The car is behind either the originally chosen door or the door that remains. It can't be anywhere else.\n",
    "- Therefore, the chance that the car is behind the door that remains is 2/3.\n",
    "- Therefore, the contestant should switch.\n",
    "\n",
    "That's it. End of story. \n",
    "\n",
    "Not convinced? Then let's simulate the game and see how the results turn out."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simulation ###\n",
    "We start by setting up two useful arrays – `doors` and `goats` – that will allow us to distinguish the three doors and the two goats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "doors = make_array('Car', 'Goat 1', 'Goat 2')\n",
    "goats = make_array('Goat 1', 'Goat 2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define a function `monty_hall` that simulates the game and returns an array of three strings in this order: \n",
    "- what is behind the contestant's original choice of door\n",
    "- what Monty throws out\n",
    "- what is behind the remaining door\n",
    "\n",
    "If the contestant's original choice is a door with a goat, Monty must throw out the other goat, and what will remain is the car. If the original choice is the door with a car, Monty must throw out one of the two goats, and what will remain is the other goat. \n",
    "\n",
    "It is clear, therefore, that the function `other_one` defined in an earlier section will be useful. It takes a string and a two-element array; if the string is equal to one of the elements, it returns the other one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def other_one(x, a_b):\n",
    "    if x == a_b.item(0):\n",
    "        return a_b.item(1)\n",
    "    elif x == a_b.item(1):\n",
    "        return a_b.item(0)\n",
    "    else:\n",
    "        return 'Input Not Valid'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the contestant's original choice is a goat, then the outcome of the game could be one of the following two:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['Goat 1', 'Goat 2', 'Car'],\n",
       "      dtype='<U6')"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "original = 'Goat 1'\n",
    "make_array(original, other_one(original, goats), 'Car')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['Goat 2', 'Goat 1', 'Car'],\n",
       "      dtype='<U6')"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "original = 'Goat 2'\n",
    "make_array(original, other_one(original, goats), 'Car')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the original choice happens to be the car, then let's assume Monty throws out one of the two goats at random, and the other goat is behind the remaining door."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['Car', 'Goat 1', 'Goat 2'],\n",
       "      dtype='<U6')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "original = 'Car'\n",
    "throw_out = np.random.choice(goats)\n",
    "make_array(original, throw_out, other_one(throw_out, goats))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now put all this code together into a single function `monty_hall` to simulate the result of one game. The function takes no arguments. \n",
    "\n",
    "The contestant's original choice will be a door chosen at random from among the three doors. \n",
    "\n",
    "To check whether the original choice is a goat or not, we first write a little function named `is_goat`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def is_goat(door_name):\n",
    "    \n",
    "    \"\"\" Check whether the name of a door (a string) is a Goat.\n",
    "    \n",
    "    Examples:\n",
    "    =========\n",
    "    \n",
    "    >>> is_goat('Goat 1')\n",
    "    True\n",
    "    >>> is_goat('Goat 2')\n",
    "    True\n",
    "    >>> is_goat('Car')\n",
    "    False\n",
    "    \"\"\"\n",
    "    if door_name == \"Goat 1\":\n",
    "        return True\n",
    "    elif door_name == \"Goat 2\":\n",
    "        return True\n",
    "    else:\n",
    "        return False\n",
    "\n",
    "\n",
    "def monty_hall():\n",
    "\n",
    "    \"\"\" Play the Monty Hall game once\n",
    "    and return an array of three strings:\n",
    "    \n",
    "    original choice, what Monty throws out, what remains\n",
    "    \"\"\"\n",
    "    \n",
    "    original = np.random.choice(doors)\n",
    "    \n",
    "    if is_goat(original):\n",
    "        return make_array(original, other_one(original, goats), 'Car')\n",
    "    \n",
    "    else:\n",
    "        throw_out = np.random.choice(goats)\n",
    "        return make_array(original, throw_out, other_one(throw_out, goats))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's play the game a few times! Here is one outcome. You should run the cell several times to see how the outcome changes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array(['Car', 'Goat 1', 'Goat 2'],\n",
       "      dtype='<U6')"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "monty_hall()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To gauge the frequency with which the different outcomes occur, we have to play the games many times and collect the results. For this, we will use a `for` loop.\n",
    "\n",
    "We will start by defining three empty arrays, one each for the original choice, what Monty throws out, and what remains. Then we will play the game `N` times and collect the results. We have set `N` to be 10,000, but you can vary that."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table border=\"1\" class=\"dataframe\">\n",
       "    <thead>\n",
       "        <tr>\n",
       "            <th>Original Door Choice</th> <th>Monty Throws Out</th> <th>Remaining Door</th>\n",
       "        </tr>\n",
       "    </thead>\n",
       "    <tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2              </td> <td>Goat 1          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2              </td> <td>Goat 1          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2              </td> <td>Goat 1          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2              </td> <td>Goat 1          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>Goat 2          </td> <td>Car           </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "</table>\n",
       "<p>... (9990 rows omitted)</p"
      ],
      "text/plain": [
       "Original Door Choice | Monty Throws Out | Remaining Door\n",
       "Goat 2               | Goat 1           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "Goat 2               | Goat 1           | Car\n",
       "Goat 2               | Goat 1           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "Goat 2               | Goat 1           | Car\n",
       "Goat 1               | Goat 2           | Car\n",
       "... (9990 rows omitted)"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Number of times we'll play the game\n",
    "N = 10000\n",
    "\n",
    "original = make_array()     # original choice\n",
    "throw_out = make_array()    # what Monty throws out\n",
    "remains = make_array()      # what remains\n",
    "\n",
    "for i in np.arange(N): \n",
    "    result = monty_hall()    # the result of one game\n",
    "    \n",
    "    # Collect the results in the appropriate arrays\n",
    "    original = np.append(original, result.item(0))\n",
    "    throw_out = np.append(throw_out, result.item(1))\n",
    "    remains = np.append(remains, result.item(2))\n",
    "    \n",
    "# The for-loop is done! Now put all the arrays together in a table.\n",
    "results = Table().with_columns(\n",
    "    'Original Door Choice', original,\n",
    "    'Monty Throws Out', throw_out,\n",
    "    'Remaining Door', remains\n",
    ")\n",
    "results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To see whether the contestant should stick with her original choice or switch, let's see how frequently the car is behind each of her two options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table border=\"1\" class=\"dataframe\">\n",
       "    <thead>\n",
       "        <tr>\n",
       "            <th>Original Door Choice</th> <th>count</th>\n",
       "        </tr>\n",
       "    </thead>\n",
       "    <tbody>\n",
       "        <tr>\n",
       "            <td>Car                 </td> <td>3388 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1              </td> <td>3234 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2              </td> <td>3378 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "</table>"
      ],
      "text/plain": [
       "Original Door Choice | count\n",
       "Car                  | 3388\n",
       "Goat 1               | 3234\n",
       "Goat 2               | 3378"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results.group('Original Door Choice')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table border=\"1\" class=\"dataframe\">\n",
       "    <thead>\n",
       "        <tr>\n",
       "            <th>Remaining Door</th> <th>count</th>\n",
       "        </tr>\n",
       "    </thead>\n",
       "    <tbody>\n",
       "        <tr>\n",
       "            <td>Car           </td> <td>6612 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1        </td> <td>1699 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2        </td> <td>1689 </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "</table>"
      ],
      "text/plain": [
       "Remaining Door | count\n",
       "Car            | 6612\n",
       "Goat 1         | 1699\n",
       "Goat 2         | 1689"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results.group('Remaining Door')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As our solution said, the car is behind the remaining door two-thirds of the time, to a pretty good approximation. The contestant is twice as likely to get the car if she switches than if she sticks with her original choice.\n",
    "\n",
    "To visualize the results, we can join the two tables above and draw overlaid bar charts."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<table border=\"1\" class=\"dataframe\">\n",
       "    <thead>\n",
       "        <tr>\n",
       "            <th>Item</th> <th>Original Door</th> <th>Remaining Door</th>\n",
       "        </tr>\n",
       "    </thead>\n",
       "    <tbody>\n",
       "        <tr>\n",
       "            <td>Car   </td> <td>3388         </td> <td>6612          </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 1</td> <td>3234         </td> <td>1699          </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "        <tr>\n",
       "            <td>Goat 2</td> <td>3378         </td> <td>1689          </td>\n",
       "        </tr>\n",
       "    </tbody>\n",
       "</table>"
      ],
      "text/plain": [
       "Item   | Original Door | Remaining Door\n",
       "Car    | 3388          | 6612\n",
       "Goat 1 | 3234          | 1699\n",
       "Goat 2 | 3378          | 1689"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "results_o = results.group('Original Door Choice')\n",
    "results_r = results.group('Remaining Door')\n",
    "joined = results_o.join('Original Door Choice', results_r, 'Remaining Door')\n",
    "combined = joined.relabeled(0, 'Item').relabeled(1, 'Original Door').relabeled(2, 'Remaining Door')\n",
    "combined"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAq4AAAEPCAYAAACDV0bQAAAABHNCSVQICAgIfAhkiAAAAAlwSFlz\nAAALEgAACxIB0t1+/AAAIABJREFUeJzt3XlYlXX+//HXAReUQlEQCCSUJRAXTC21TNTGJS0yMzQt\nywWzGp00c0tNs1BzSUdzRtHSMRtzSSND85v7bjPauAKuoCm4BCoGCpzfH16cn0dcAA8ebno+rosr\n+dyfc9/v9+Hq+PLmc9+3KS0tzSwAAACghHOwdwEAAABAQRBcAQAAYAgEVwAAABgCwRUAAACGQHAF\nAACAIRBcAQAAYAgEVwAAABgCwRUAAACGQHDFHSUmJtq7BJsrjT1J9GUkpbEnqfT2BaBkIbgCAADA\nEAiuAAAAMASCKwAAAAyB4AoAAABDILgCAADAEMrYu4DSZMeeQ/YuwabS09N14Uq2vcuwqdLYk1Q8\nfXl7uKn6I+423ScAAPeD4GpDU+d+Z+8SbCojI0POzs72LsOmSmNPUvH09V6vjgRXAECJwlIBAAAA\nGALBFQAAAIZAcAUAAIAhEFwBAABgCARXAAAAGALBFQAAGEpSUpJcXV31zTffFOn1derU0TvvvGPj\nqqx9/fXXcnV1VXJycrEe58+G4AoAAB6IU6dO6b333lPdunXl4eGhwMBAde/eXbt27Sr0vkwmU5Hr\ncHBwuK/XF4TJZCrQMbZs2SJXV1fLl4eHh4KCgtShQwdNmTJFFy5cKNY6jYb7uAIAUMIl/3ZOp1PO\n27WG+30oyY4dO/TKK6/IZDLpjTfeUGBgoFJSUrRo0SK1bdtWEyZMUJ8+fQq0L19fX509e1Zly5Yt\nUi2//PKLHBxK1rm7qKgoNWzYUDk5OTp//rx27dql8ePHa+bMmfryyy/1zDPP2LvEEoHgCgBACXc6\n5bzdH3JzPw8lSUtLU48ePVSxYkWtWbNGjz76qGXbO++8o5deeknDhg1TWFiYGjVqdMf9XL9+XQ4O\nDnJ0dFS5cuWKVIukIgfe4tS4cWN17NjRauzAgQPq2LGjevTooZ07d6patWoPrJ6b3+uSpGT9cwMA\nAJQ6X375pc6dO6ePP/7YKrRKkpOTk7744gtJ0sSJEy3jeb9CX7JkiaKjo1W7dm15eXnpt99+u+Ma\n182bNys8PFyenp56/PHH9dVXXyk6Olqurq5W825d45p3rGXLlmny5MkKDQ2Vp6enIiIidPz4cavX\nbt++XT179lSdOnXk4eGh4OBgDRgwQGlpaTZ5r24WGhqq6OhopaWlafbs2Vbb9u/fr86dO8vX11fe\n3t7q0KGDtm/fnm8fSUlJevPNN1WzZk15eXmpZcuWWrVqldWcu73XJQ1nXAEAQLFas2aNnJyc9OKL\nL952u5+fnxo3bqxNmzYpKytL5cuXt2ybMmWKHBwc1K9fP5nNZj300EO6fPlyvn38+uuv6ty5szw8\nPDR8+HDl5OTos88+k6ura761pndaezpt2jSVKVNGf/3rX3Xp0iVNmzZNUVFRWrt2rWXOihUrdPny\nZb355ptyc3PTgQMHtGDBAh0+fFhr1qwpyttzVxEREfrrX/+q9evX68MPP5QkJSQkqF27dnrooYc0\nYMAAlStXTgsWLNCLL76oFStWqEmTJpKk8+fPq3Xr1rp69ar69u2rqlWr6ttvv9Vrr72mmJgYvfTS\nS1bHut17XdIQXAEAQLE6fPiwAgIC7vor+tq1a2vbtm06duyYQkJCLONXrlzR7t275eTkZBm7XXCN\njo6Wg4OD1qxZI09PT0lSx44d77r04FbXrl3T+vXrLb8er1SpkoYNG6bDhw8rODhYkjRmzBirWiSp\nUaNGioqK0s6dO/Xkk08W+HgFUaZMGfn7+1ud+R07dqyuXbumuLg4+fn5SZK6deumRo0aacSIEVq3\nbp2kG0E0NTVVP/zwg5o2bSpJeuONNxQeHq4RI0YoIiLCainA7d7rkoalAgAAoFhduXLlnmfv8rbf\nGkq7du16zyCVm5urTZs2qV27dpbQKt04k/vss88WuM6uXbtaBbkmTZrIbDbrxIkTlrFbA/TFixfV\nqFEjmc1m7d27t8DHKoyHHnpIV65ckXSj1/Xr16tdu3aW0CpJVapU0auvvqq9e/fq3LlzkqS1a9eq\nXr16ltCaV3+vXr2UkpKiX3/91eo4BXmv7Y3gCgAAitXNwetO8rbfGnBvDmd3cu7cOf3xxx+qUaNG\nvm01a9YscJ3e3t5W31euXFmSrNavnj59Wj179pSvr698fX3l7++vsLAwmUwmXbp0qcDHKoybg//5\n8+d19epVBQQE5JsXFBQkSZZ7xyYnJyswMPC288xms5KSkqzGC/Je2xtLBQAAQLF67LHHtG/fPl2/\nfv2OywX279+vsmXLyt/f32q8QoUKD6JESbrjFfRms1nSjbOdHTt21MWLFzVo0CAFBgbK2dlZubm5\neumll5Sbm2vzmrKzs3X06FHVqlXLqpbi8CDf66IiuAIAgGLVpk0b7d69WytWrFDnzp3zbT958qR2\n7Nihli1bWl2YVVDu7u6qUKFCvjsASNLRo0eLVPPtHDhwQImJifrHP/6hyMhIy/ixY8dsdoxbrVix\nQn/88YdatWol6UavFStWVGJiYr65CQkJkqTq1atb/nuneSaTSb6+vsVWd3FhqQAAAChWeVfgjx49\nWidPnrTalpmZabk11eDBg4u0fwcHBz3zzDOKi4vTmTNnLOPHjh3Tzz//XPTCb5F3RvbWM6vTp08v\nlidx7du3T8OGDVOVKlXUu3dvSTd6bdWqlVavXm219vb333/Xv//9bz3++ONyd79xv902bdro119/\n1Y4dOyzzsrKyNG/ePHl4eCgsLMzmNRc3zrgCAIBi5erqqvnz5ysyMlLPPPOMevTooaCgIKWkpOib\nb77R8ePHNWHChELdAeBWQ4cO1fr169WmTRv16tVLOTk5iomJUUhIiPbv32+TPoKCguTv768RI0bo\n9OnTcnV11dq1a3XmzJn7/hX+tm3bdP36deXk5OjixYvauXOn4uLiVLlyZS1cuNASRiXpww8/1IYN\nG9S2bVv17t1b5cuX14IFC3Tp0iWNGzfOMu9vf/ubli1bpldeeUVRUVFyc3PT4sWLlZCQoJiYmBL3\n9LCCILgCAIBi16RJE23dulVTpkzRihUrlJKSIhcXFzVu3FizZs26bWi921nMW7eFhYVp6dKlGjly\npKKjo+Xt7a3hw4crPj5eR44cyffagt7b9ebxMmXKaPHixRo6dKj+/ve/y9HRUc8++6xmzJihoKCg\nIp91NZlMmjt3rubOnauyZcuqUqVKCgoK0ogRI/T666+rSpUqVvODgoIUFxensWPHavr06crNzVX9\n+vX197//XY0bN7bMc3Nz05o1a/TRRx9p3rx5+uOPPxQSEqJ//etfeu655wrUf0ljSktLK75Vvn8y\nke9+au8SbCojI0POzs72LsOmSmNPUvH09V6vjmpcP+TeE4tRYmLiba+INbLS2JNUevsqKZJ/O6fT\nKeftWoO3h1uRH/lqT926dVN8fLx++eUXe5cCG+CMKwAAJVz1R9wNGRoftFufunX06FGtXbtW3bp1\ns2NVsCWCKwAAKBXCwsLUtWtX+fn5KSkpSfPmzZOTk5P69+9v79JgIwRXAABQKrRq1UrLli1Tamqq\nypUrpyeffFIjR4687YMJYEwEVwAAUCrMmDHD3iWgmBnvPgiFdO7cOQ0ZMkT169eXh4eHQkND9cor\nr2jt2rX2Lg0AAACFUKrPuCYlJalNmzZycXHRRx99pNq1ays3N1cbNmzQwIEDtW/fvkLv02w2y2w2\nG/LeZwAAAEZWqtPXoEGDZDKZtGHDBkVERMjf31+BgYHq06ePtmzZIkmaOXOmnnrqKXl7e6tWrVrq\n37+/0tPTLftYtGiRfHx8tHbtWjVt2lTVqlWzPFINAAAAD06pDa5paWn6+eefFRUVpQoVKuTbXqlS\nJUk3Ht82fvx47dixQzExMfrvf/+rIUOGWM3NzMzU5MmT9fnnn2vnzp2WZwADAADgwSm1SwWOHTsm\ns9l8zxtiv/XWW5Y/V69eXWPGjFG3bt30j3/8wzKem5uriRMnqm7dusVWLwAAAO6u1AbXgj4zeOPG\njfr888+VkJCgS5cuKScnR9euXVNKSoo8PDwk3XjEW506dYqzXAAAANxDqQ2u/v7+MplMSkhIUPv2\n7W87Jzk5WV26dNEbb7yhESNGqEqVKtq7d6969+6ta9euWeaVL1++QM/wnTIg/3OWAaOqUD5Vl3+z\n7yMmPZ2ly7+dtWsNtlYae5JKRl/XVFkXM5xstj8eYQuUPKU2uFauXFmtWrXSnDlz1LdvX1WsWNFq\ne3p6uvbs2aPr16/r008/tQTTH3/8scjHDHQqXbfYysjIkLOzs73LsKnS2JNUjH3l2H6XhVEaf16l\nsSepZPSVVbW7qj5C2ARKs1J7cZYkTZo0SWazWS1atNDKlSt15MgRJSYmau7cuXr66acVEBCgnJwc\nzZw5UydPntTSpUut1rYCAABIkqurqyZMmFCk17Zv314dOnSwcUV/TqU6uD766KPauHGjwsPD9dFH\nH+npp59WRESE1qxZo2nTpqlWrVqaMGGCZs2apSZNmmjhwoX65JNP7F02AAClyqJFi+Tq6mr5cnNz\nU61atfT222/rzJkz9i6vQEwmU4GWDd7ptfa6//vbb79t9d77+PgoLCxMPXr00Pfff1/ga4JKClNa\nWpqxKi7BKiZ/YO8SbKok/OrP1kpjTxJ9GUlp7EkqGX1lVe2unIql8+4vpmtn5ZCdatcacstUk7mc\nZ5Feu2jRIr377rsaNmyY/Pz8lJmZqV9++UVff/21Hn30UW3fvl3lypWzccW2de3aNZUpU6ZIATQ7\nO1vSjYu9H7S3335by5Yt08yZM2U2m/XHH38oOTlZq1ev1v79+/X000/rm2++0UMPPfTAayuKUrvG\nFQCA0sIhO1XlLyy0aw1ZVbsrp4jBNU/Lli3VoEEDSdJrr72mKlWqaNq0aYqLi1NERIQtyiw29xOs\n7RFYb+bg4KCXX37ZamzEiBGaNm2aPvroIw0YMEBz5859oDVdvXo13/VHBVGqlwoAAICSq0mTJjKb\nzTp+/Hi+bevWrVP79u3l4+MjHx8fvfzyy/ke1d6vXz95enrq1KlTioyMlI+Pj0JCQvTPf/5TknTo\n0CFFRETI29tbtWvX1uLFi61en5aWppEjR+qpp55S9erV5ePjow4dOmj79u356rl1jWve8oetW7dq\n+PDhCggIkLe3t7p3766LFy9avbZ9+/Z6/vnnLd8nJSXJ1dVV06ZN04IFC1S/fn15eHioZcuW2rNn\nT75jr1ixQo0bN5anp6eaNm2q2NhY9evX777vLz9gwAC1bNlSK1as0NGjR622zZs3T02bNpWnp6ce\ne+wxvffee0pLS8u3j5UrV6pFixby8vJSzZo11bt3b506dcpqTt7PKSkpSV26dJGvr68iIyOLVDPB\nFQAA2MXJkycl3bgT0M2WLFmil19+WRUqVNDo0aM1dOhQnTx5Uu3bt9eRI0cs80wmk8xms1555RV5\ne3tr7Nix8vPz07Bhw/T111/r5ZdfVlhYmMaMGSMXFxe98847OnHihOX1J06cUGxsrFq3bq1x48bp\ngw8+UEpKijp27KiDBw8WqIdhw4bp4MGDGjp0qHr16qXVq1dr8ODBVnPutDZ2+fLlmj59unr27KkP\nP/xQSUlJeu2115ST8/9v6bJmzRr17NlTZcuW1ahRo/T888+rf//++vXXX4u85vZmkZGRys3N1YYN\nGyxjn332mQYNGiQPDw99/PHH6tSpkxYuXKgXXnhB169ft8xbvHix3njjDTk4OGj06NHq2bOn1qxZ\no7Zt2+r333+36t9sNuull15SpUqV9PHHHxc5uLJUAAAAPBCXLl3SxYsXlZmZqd27d2vixImqUKGC\n2rRpY5lz9epVffDBB+revbumT59uGX/ttdfUsGFDTZw4UbNnz7aMX79+XZ06ddKgQYMkSZ06dVJI\nSIj69++vOXPm6KWXXpIkhYeHq1GjRvr66681YsQISVJoaKj27t1rVWOPHj3UqFEj/fOf/9S0adPu\n2ZObm5uWL19u+T4nJ0ezZ8/W5cuX9fDDD9/1tadOndKePXvk4uIiSQoICFC3bt30888/q3Xr1pKk\nMWPGyNPTU2vWrLH8ar158+Zq3769fH1971nfvYSEhEiS5az3hQsXNGnSJLVo0ULLli2zhOPatWvr\nnXfe0fz589W7d29lZ2dr1KhRCgkJ0Y8//qjy5ctLklq0aKEOHTpo6tSpGjt2rOU4169fV7t27fTx\nxx/fV72ccQUAAMUu74ybv7+/QkND9cYbb+ihhx7SN998Iy8vL8u89evXKz09XZ06ddLFixctX9nZ\n2WrSpIk2b96cb9+vvfaa5c+VKlVSQECAnJycLKFVuhEKK1WqZDnLK0lly5a1/DkrK0u///67srOz\nVb9+/XyB9nZMJpO6d+9uNdakSRPl5OQoOTn5nq9/8cUXLaE177Vms9lyVvjs2bM6dOiQunTpYrUe\ntGnTpqpVq9Y9918QeRdlXblyRdKN9//69et66623rM7odunSRdWqVdNPP/0kSdqzZ49SU1PVs2dP\nS2iVpKeeekphYWGWeTfr1avXfdfLGVcAAFDsTCaTJk6cqMDAQF26dEmLFi3Stm3brEKPJB09elRm\ns1kvvvjibffh6OhoNVa2bFlVq1bNaszFxcUqDN88fvM6TbPZrM8//1zz58+3CrSS5OfnV6C+fHx8\nrL7PW/Zwu/Wgt/L29r7ra/PC7+1qqVmzpv73v/8VqMa7yQuseQE2b31qQECA1TwHBwfVrFlTSUlJ\nltpMJlO+eZIUFBSk2NjYfK+3xRligisAAHgg6tevb7mrQPv27dW+fXv17t1bu3fvVoUKFSRJubm5\nMplMmjVrljw9730XgzvdnurWgJvn5vuWTp48WZ988om6deumkSNHqkqVKnJwcNCUKVOs1sLeTUGO\nUxyvtZVDhw5JuhGEC3PswtZYtmxZm9zLluAKAAAeuLwLetq1a6fZs2drwIABkqQaNWrIbDaratWq\nat68ebHWsHLlSjVr1kwzZsywGo+Oji7W4xZU9erVJem2d104duyYTY7x73//Ww4ODmrRooUkydfX\nV2azWYmJiZYwK90IqseOHVO9evXyzQsPD7faZ2Jiok3Ort4Oa1wBAIBdNG7cWE888YRmzZqla9eu\nSbpxr9dKlSpp8uTJVlew57lw4YLNju/o6JjvzOHOnTu1a9cumx3jfnh6eiokJETffvutMjIyLONb\ntmwp8F0P7mbq1Klav369OnXqpBo1aki6cXFVuXLl9M9//tPqvVm8eLFSU1PVtm1bSTfOnlerVk1f\nfvml5WcnSdu2bdOePXss82yNM64AAKDY3elXy++++65ef/11LVy4UD179tTDDz+sqVOnKioqSs88\n84w6deqkatWqKTk5WT///LNCQkI0c+ZMm9TUrl07jR8/Xm+99ZaaNm2qI0eOaP78+QoODrYKioXt\nyZa/6h81apS6deum1q1bq1u3bkpLS1NMTIxq1apVoBqlG8svvv32W0lSZmamkpOTFRcXp4MHD6p5\n8+aaOnWqZW6VKlU0ePBgffrpp+rYsaPat2+v48ePKyYmRnXr1rVcCFemTBmNHTtW/fr1U9u2bfXK\nK6/o/Pnzmj17try9vS1n0G2N4AoAQAmXW6aasqp2v/fEYq7hftzpnqMdOnRQzZo1NX36dMs9QTt2\n7CgvLy9NmTJFM2fOVFZWljw9PfXkk0/qzTffLNB+bzduMpmsxgcOHKjMzEwtWbJE33//vUJCQvTl\nl19q6dKl2rZt211fW5Rj32t/txtv27at5s6dq/Hjx2vs2LHy9/fXF198oW+++Ubx8fG3Pf6t8u4S\nIEkVK1aUm5ubwsLCNHToUHXo0CHf/Pfff19ubm6aPXu2Ro4cqUqVKql79+4aNWqU1Z0YIiMjVbFi\nRU2dOlVjxoyx3Nps9OjRcnV1ved7UhSmtLS0B7cCuJSrmPyBvUuwqZLw7HFbK409SfRlJKWxJ6lk\n9JVVtbtyKt7fk4QAo2jWrJnc3d2t7iH7Z8AaVwAAgBIqJydHubm5VmObN2/W/v371axZMztVZT8s\nFQAAACihTp8+rY4dO6pz587y8vJSfHy8vvrqK3l5eeVbNvFnQHAFAAAooSpXrqywsDAtXLhQ58+f\nV8WKFdW2bVuNGjXK8sCCPxOCKwAAQAnl4uKiuXPn2ruMEoM1rgAAADAEgisAAAAMgeAKAAAAQyC4\nAgAAwBAIrgAAADAEgisAAAAMgeAKAAAAQyC4AgAAwBAIrgAAADAEgisAAAAMgUe+2lBW1e72LsGm\nrjimq0zlSvYuw6ZKY08SfRlJaexJKhl95ZapZtfjAyh+BFcbyqlY194l2NTZ04l6+JFAe5dhU6Wx\nJ4m+jKQ09iSV3r4AlCwsFQAAAIAhEFwBAABgCARXAAAAGALBFQAAAIZAcAUAAIAhEFwBAABgCARX\nAAAAGALBFQAAAIZAcAUAAIAhEFwBAABgCARXAAAAGALBFQAAAIZAcAUAAIAhEFwBAABgCGXsXUBp\nsmPPIXuXYFPp6em6cCXb3mXYVGnsSSpaX94ebqr+iHsxVQQAgO0RXG1o6tzv7F2CTWVkZMjZ2dne\nZdhUaexJKlpf7/XqSHAFABgKSwUAAABgCARXAAAAGALBFQAAAIZAcAUAAIAhEFwBAABgCARXAAAA\nGALBFQAAAIZAcAUAAIAhEFwBAABgCARXAAAAGALBFQAAAIZAcAUAAIAhEFwBAABgCARXAAAAGALB\nFQAAAIZAcAUAAIAhlCnKiy5fvqwVK1bo5MmTSktLk9lsttpuMpk0adIkmxQIAAAASEUIruvWrdMb\nb7yhy5cv33EOwRUAAAC2VujgOmTIELm4uGj+/Plq0KCBXFxciqMuAAAAwEqh17ieOnVKf/3rX9Wi\nRQtCKwAAAB6YQgfX2rVrKz09vThqAQAAAO6o0MF17Nixmjt3rnbv3l0c9QAAAAC3Veg1rk2aNFF0\ndLTatWsnf39/eXt7y9HR0WqOyWTSt99+a7MiAQAAgEIH1++++059+/ZVTk6OUlJS9Mcff+SbYzKZ\nbFIcAAAAkKfQwXXs2LEKDAzUggULFBAQUBw1AQAAAPkUeo1rSkqKevbsabPQeu7cOQ0bNkwNGjSQ\np6engoKC1LZtW82ePVsZGRk2OUaeunXrasaMGfecFxsbq06dOikgIECurq7aunWrTesAAABA4RX6\njGv9+vWVlJRkk4MnJSWpTZs2qlSpkkaOHKlatWrJyclJhw8f1oIFC1S1alV16tTJJscqjKtXr+rJ\nJ59UZGSk3nrrrQd+fAAAAORX6OD62WefKTIyUnXq1FHnzp3v6+ADBw6Uo6OjNmzYICcnJ8u4r6+v\nWrdubTX31KlTGjJkiDZt2iRJCg8P14QJE/TII49Ikk6cOKHhw4frP//5j65cuaKAgAANHz5cbdq0\nkSR16NBBycnJGjVqlEaOHCmTyaSLFy/etq7IyEhJ0sWLF/M9zhYAAAD2UeilAm+++aauX7+uvn37\nytvbWw0bNtSTTz5p9dW4ceN77ictLU3r1q1TVFSUVWi9k1dffVUXLlxQbGysYmNjdfbsWXXv3t2y\n/cqVK2rdurVWrlyprVu3KiIiQq+//rqOHDkiSVq4cKG8vb01ZMgQJSQkKD4+vrCtAwAAwI4KfcbV\nzc1N7u7u973G9ejRozKbzfL397caDw0NtTzgIDIyUpMnT9b69et18OBB7d27Vz4+PpKkOXPm6PHH\nH9fGjRvVvHlz1a5dW7Vr17bsZ+DAgYqLi9PKlSs1aNAgVa5cWQ4ODnJ2dpa7u/t91Q4AAIAHr9DB\nddWqVcVRh0VcXJxyc3PVv39/ZWZmSpISEhLk6elpCa2S5OfnJy8vL8XHx6t58+a6evWqxo8fr59+\n+klnz55Vdna2srKyrMJscZsyoNEDO9afVUa2i46nsHzDFso7mpWYmGjvMu7JCDUWVmnsSSp9fQUG\nBtq7BAC3KHRwtZWaNWvKZDLl+6Dz9fWVJFWoUMEyZjab73hv2LzxDz/8UOvWrdO4ceNUs2ZNVaxY\nUX379tW1a9eKqYP8Ap3WPrBjPQgZGRlydna2dxlWsqp2V1BI3SK/PjExsVT+ZURfxlEae5JKb18A\nSpZCr3GVbly0NG7cOLVp00aPP/64du3aZRmfMGFCgdaPurq6qmXLlpozZ849b3sVHBysM2fOKDk5\n2TJ24sQJnTlzRsHBwZKkHTt2qEuXLurQoYNq1aolT09PHT9+3Go/5cqVU25ubmHbBQAAQAlQ6OB6\n8uRJPf3005oxY4auX7+uEydOWJ6eVaVKFS1fvlwxMTEF2tekSZOUm5urFi1aaNmyZYqPj9fRo0e1\ndOlSHThwwPIo2fDwcIWGhioqKkp79+7Vnj17FBUVpfr166tZs2aSpICAAP3www/69ddfdeDAAfXt\n21dZWVlWx/P19dW2bdt05syZO95RQLpx4di+fft04MABSTfW4+7bt0+pqamFfbsAAABgI4UOrqNH\nj5bZbNaOHTu0ZMmSfLeLeu6557Rx48YC7cvPz0+bNm1Sq1at9Omnn6p58+YKDw/XrFmz1KdPH0VH\nR1vmLlq0SFWrVtULL7ygiIgIeXp6auHChZbtn3zyidzd3dW+fXtFRkaqUaNGatKkidXxhg8frtOn\nT6t+/fp3vbjsxx9/1DPPPKOIiAiZTCb97W9/U/PmzfXll18WqC8AAADYXqHXuG7YsEH9+/eXn5/f\nbc9aPvroo/rtt98KvD93d3dFR0dbhdTb8fb2tgqqt6pevbq+++47q7F3333X6vuGDRtq8+bN96zp\n1Vdf1auvvnrPeQAAAHhwCn3GNSsrS5UrV77j9vT0dDk4FGnpLAAAAHBHhU6YISEh2rp16x23r1q1\nSnXrFv2qbwAAAOB2Ch1c+/Xrp++++06TJk3S77//LknKzc1VQkKCevfurV9++UXvvPOOzQsFAADA\nn1uh17jk21ynAAAUGklEQVR27txZp06d0qeffqpPP/1UktSpUydJkoODg8aMGaN27drZtkoAAAD8\n6RXpAQTvvfeeOnfurO+//17Hjh1Tbm6uatSooeeff15+fn42LhEAAAAoQnBNTk6Wm5ubfHx89Pbb\nb+fb/scff+j8+fOqXr26TQoEAAAApCKsca1Xr55++OGHO26Pi4tTvXr17qsoAAAA4FaFDq63PnDg\nVtnZ2TKZTEUuCAAAALidIt1w9U7BND09Xf/3f/8nd3f3+yoKAAAAuFWBguv48eNVpUoVValSRSaT\nSVFRUZbvb/6qUaOGlixZYrnLAAAAAGArBbo4q0GDBurVq5ckKSYmRi1atJC/v7/VHJPJJGdnZ4WF\nhemFF16wfaUAAAD4UytQcP3LX/6iv/zlL5KkjIwM9ezZUw0bNizWwgAAAICbFSi4/uc//7H8Oe/M\n681jt9OgQYP7KAsAAACwVqDg+uyzzxb4TgFms1kmk0kXL168r8IAAACAmxUouM6cObO46wAAAADu\nqkDB9dVXXy3uOgAAAIC7KtJ9XAEAAIAHjeAKAAAAQyC4AgAAwBAIrgAAADAEgisAAAAMgeAKAAAA\nQyC4AgAAwBAIrgAAADAEgisAAAAMgeAKAAAAQyjQI19RMFlVu9u7BJu64piuMpUr2bsMK7llqtm7\nBAAAYCcEVxvKqVjX3iXY1NnTiXr4kUB7lwEAACCJpQIAAAAwCIIrAAAADIHgCgAAAEMguAIAAMAQ\nCK4AAAAwBIIrAAAADIHgCgAAAEMguAIAAMAQCK4AAAAwBIIrAAAADIHgCgAAAEMguAIAAMAQCK4A\nAAAwBIIrAAAADKGMvQsoTXbsOWTvEmwqPT1dF65k27sMmyqNPUm278vbw03VH3G32f4AALAFgqsN\nTZ37nb1LsKmMjAw5OzvbuwybKo09Sbbv671eHQmuAIASh6UCAAAAMASCKwAAAAyB4AoAAABDILgC\nAADAEAiuAAAAMASCKwAAAAyB4AoAAABDILgCAADAEAiuAAAAMASCKwAAAAyB4AoAAABDILgCAADA\nEAiuAAAAMASCKwAAAAyB4AoAAABDILgCAADAEAiuAAAAMASCKwAAAAyB4AoAAABDILgCAADAEAiu\nAAAAMASCKwAAAAyB4AoAAABDILgCAADAEOweXM+dO6dhw4apQYMG8vT0VFBQkNq2bavZs2crIyPD\npseqW7euZsyYcdc52dnZGj16tJ566il5e3srODhYffr00alTp2xaCwAAAAqnjD0PnpSUpDZt2qhS\npUoaOXKkatWqJScnJx0+fFgLFixQ1apV1alTpwda09WrV7Vv3z598MEHql27ti5duqThw4erc+fO\n2rp1qxwc7J71AQAA/pTsGlwHDhwoR0dHbdiwQU5OTpZxX19ftW7d2mruqVOnNGTIEG3atEmSFB4e\nrgkTJuiRRx6RJJ04cULDhw/Xf/7zH125ckUBAQEaPny42rRpI0nq0KGDkpOTNWrUKI0cOVImk0kX\nL17MV5OLi4uWL19uNfb555+rcePGio+PV0hIiE3fAwAAABSM3U4fpqWlad26dYqKirIKrXfy6quv\n6sKFC4qNjVVsbKzOnj2r7t27W7ZfuXJFrVu31sqVK7V161ZFRETo9ddf15EjRyRJCxculLe3t4YM\nGaKEhATFx8cXuNZLly7JZDKpcuXKhW8UAAAANmG3M65Hjx6V2WyWv7+/1XhoaKjS09MlSZGRkZo8\nebLWr1+vgwcPau/evfLx8ZEkzZkzR48//rg2btyo5s2bq3bt2qpdu7ZlPwMHDlRcXJxWrlypQYMG\nqXLlynJwcJCzs7Pc3d0LXOf169f14Ycfql27dvLy8rJB5wAAACgKuy4VuJ24uDjl5uaqf//+yszM\nlCQlJCTI09PTElolyc/PT15eXoqPj1fz5s119epVjR8/Xj/99JPOnj2r7OxsZWVlWYXZwsrJyVGf\nPn10+fJlLV68+L57AwAAQNHZLbjWrFlTJpNJiYmJVuO+vr6SpAoVKljGzGazTCbTbfeTN/7hhx9q\n3bp1GjdunGrWrKmKFSuqb9++unbtWpHqy8nJUc+ePXX48GGtWrWqQMsEpgxoVKRjoeAysl10PMVs\n7zJKvfKO5nz/b9pLSanDlkpjT1Lp6yswMNDeJQC4hd2Cq6urq1q2bKk5c+aoT58+cnZ2vuPc4OBg\nnTlzRsnJyapevbqkGxdjnTlzRsHBwZKkHTt2qEuXLurQoYMkKTMzU8ePH1dAQIBlP+XKlVNubu49\na8vOztabb76p+Ph4rVq1Sm5ubgXqKdBpbYHmGUVGRsZdfy72kFW1u4JC6hb59YmJiaXyLyP6Mo7S\n2JNUevsCULLY9d5OkyZNUm5urlq0aKFly5YpPj5eR48e1dKlS3XgwAE5OjpKunEHgdDQUEVFRWnv\n3r3as2ePoqKiVL9+fTVr1kySFBAQoB9++EG//vqrDhw4oL59+yorK8vqeL6+vtq2bZvOnDlz2zsK\nSDfOtL7++uv673//q5iYGJnNZqWmpio1NdWydAEAAAAPnl2Dq5+fnzZt2qRWrVrp008/VfPmzRUe\nHq5Zs2apT58+io6OtsxdtGiRqlatqhdeeEERERHy9PTUwoULLds/+eQTubu7q3379oqMjFSjRo3U\npEkTq+MNHz5cp0+fVv369a3OxN7s9OnTWr16tc6cOaPw8HAFBwdbvr777rvieSMAAABwT6a0tDQW\nDNpIxeQP7F2CTZXUpQI5FVkqcCv6Mo7S2JNUevsCULLwGCgAAAAYAsEVAAAAhkBwBQAAgCEQXAEA\nAGAIBFcAAAAYAsEVAAAAhkBwBQAAgCEQXAEAAGAIBFcAAAAYAsEVAAAAhkBwBQAAgCEQXAEAAGAI\nBFcAAAAYAsEVAAAAhkBwBQAAgCEQXAEAAGAIBFcAAAAYAsEVAAAAhkBwBQAAgCEQXAEAAGAIBFcA\nAAAYAsEVAAAAhkBwBQAAgCEQXAEAAGAIBFcAAAAYAsEVAAAAhkBwBQAAgCGUsXcBpUlW1e72LsGm\nrjimq0zlSvYuw0pumWr2LgEAANgJwdWGcirWtXcJNnX2dKIefiTQ3mUAAABIYqkAAAAADILgCgAA\nAEMguAIAAMAQCK4AAAAwBIIrAAAADMGUlpZmtncRAAAAwL1wxhUAAACGQHAFAACAIRBcAQAAYAgE\nVwAAABgCwRUAAACGQHC9DzExMapXr548PT0VHh6u7du327skK9u2bVPXrl1Vq1Ytubq66ptvvsk3\nJzo6WiEhIfLy8lKHDh10+PBhq+1paWmKioqSr6+vfH191bdvX6Wnp1vNOXjwoNq3by8vLy+FhoZq\n4sSJxdbTlClT1LJlS/n6+iogIEBdunTRoUOHDN9XTEyMnnrqKUs9rVu31k8//WTonm41efJkubq6\n6oMPPrAaN1pf48ePl6urq9VXcHCwoXvKk5KSon79+ikgIECenp5q0qSJtm3bZtje6tatm+9n5erq\nqsjISMuce32OX7t2TYMHD5a/v7+8vb3VtWtX/fbbb1ZzTp06pcjISHl7e8vf319DhgxRdnZ2sfQE\n/NkRXIto+fLlGjZsmN5//31t3rxZTzzxhDp37qzTp0/buzSLjIwMhYaGavz48apYsWK+7Z9//rlm\nzZqlzz77TOvXr5e7u7s6duyojIwMy5zevXtr//79Wr58uZYvX67//e9/euuttyzbL1++rI4dO8rT\n01MbNmzQ+PHj9fe//10zZ84slp62bdumPn366KefflJsbKzKlCmjF198UWlpaYbuy9vbW2PHjtWm\nTZu0YcMGPfPMM+rWrZsOHjxo2J5utnv3bi1YsEC1a9e2GjdqX0FBQUpMTFRCQoISEhKswp1Re0pP\nT1ebNm1kMpm0dOlS7dq1SxMmTJC7u7the9uwYYPlZ5SQkKCNGzfKZDLppZdeklSwz/GhQ4dq1apV\nmjdvnuLi4nT58mVFRkbKbL5xJ8nc3Fy98sorunr1qlavXq158+bp+++/14gRI2zeDwDu41pkzz77\nrOrUqaOpU6daxho0aKAXX3xRI0eOtGNlt+fj46PPPvtMXbt2tYwFBwerb9++eu+99yRJmZmZCgwM\n1Lhx49SjRw/Fx8ercePG+umnn9SoUSNJ0o4dO9SuXTv98ssv8vf319y5czVmzBgdOXJE5cqVkyRN\nmjRJX375pQ4cOFDsfWVkZMjX11eLFi1SmzZtSk1fklSjRg199NFH6tGjh6F7Sk9PV3h4uKZPn64J\nEyaoVq1aljNsRuxr/Pjx+v777/OdicxjxJ4kaezYsdq+fbvi4uLuOMeoveWZNGmSZsyYocOHD8vJ\nyemen+OXLl1SQECAZs2apU6dOkmSTp8+rTp16mjZsmVq0aKF1q5dqy5dumj//v3y8vKSJH377bca\nMGCAEhMT9dBDDxVrT8CfDWdci+D69evau3evwsPDrcZbtmypnTt32qeoQjpx4oRSUlLUokULy5iT\nk5OaNm1q6WHXrl16+OGHLX8BSVLjxo3l7OxsmbN79241adLE8heQJLVq1UpnzpxRUlJSsfdx+fJl\n5ebmqnLlyqWmr9zcXC1btkxXr17Vk08+afie/va3v6ljx45q1qyZ1biR+zp58qRq1aqlevXqqVev\nXjpx4oThe/rxxx/VoEED9ezZU4GBgWrWrJnmzJlj2W7k3vIsXLhQkZGRcnJyKtDn+J49e5SdnW3V\ns7e3tx577DGrfh577DFLaM3rJzMzU3v37i3WfoA/I4JrEVy4cEE5OTmqVq2a1bi7u7tSU1PtVFXh\npKamymQyWf0aULLu4dy5c6patWq+17q5uVnmpKam3vZ9MJvND+S9GDp0qOrVq6cnnnjCUo9R+zp4\n8KB8fHxUrVo1DRo0SAsXLlRwcLChe5o/f75OnDhx21+bGrWvRo0a6YsvvtDSpUs1ffp0paSkqG3b\ntkpLSzNsT9KNYDp37lzVqFFDy5cvV79+/TRmzBjFxMRYajJqb5K0bt06JSUl6fXXX5dUsM/xc+fO\nydHRUVWqVLnjnNTU1HzvSdWqVeXo6GiYvw8AIylj7wKMzGQyWX1vNpvzjZV09+rhdv3ca07e2q/i\nfi+GDx+uXbt2afXq1fmOZcS+goKCtGXLFqWlpSk2NlZvvfWWVq1addd6SnJPR44c0ccff6zVq1fL\n0dHxjvOM1lerVq2svm/YsKHCwsK0aNEiNWzY8I71lOSepBtn+hs0aGBZ6lSnTh0dPXpUMTEx6t27\n913rKum9STf+EfX4448rNDTUarwon+MF6flu4wCKjjOuRZD3r+mUlBSr8fPnz+f7l3dJVa1atdue\n4bi5h2rVqun8+fP5XnvhwgWrObd7H253ZsaWhg0bpu+++06xsbHy9fW1jBu5rzJlysjPz09hYWEa\nOXKk6tSpoy+++MKwPe3atUsXL15U48aN5ebmJjc3N23dulUxMTFyd3dXlSpVDNnXrZydnRUcHKxj\nx44Z9mclSR4eHgoKCrIaCwoK0qlTpyw1GbW38+fPKy4uTj169LCMFeRzvFq1asrJydHFixfvOufW\n9yTvbK5R/j4AjITgWgRly5ZVWFiYNmzYYDW+fv16NW7c2D5FFZKfn588PDy0fv16y1hmZqa2b99u\n6eGJJ57QlStXtHv3bsucnTt3WtZe5s3Zvn27rl27Zpmzbt06eXl5WQVKWxoyZIiWL1+u2NhY+fv7\nl5q+bpWbm6usrCzD9tShQwdt27ZNW7ZssXzVr19fL7/8srZs2aKAgABD9nWrzMxMJSYmytPT07A/\nK+nGWtTExESrscTERFWvXl2Ssf/f+vrrr+Xk5GS5m4B098/xvFrDwsJUpkwZq55Pnz5tuQgtr5/4\n+HidOXPGqh8nJyeFhYUVSz/An5nj0KFDP7J3EUb08MMPKzo6Wh4eHqpQoYImTpyoHTt2aMaMGXJx\ncbF3eZJuXHEfHx+vlJQU/etf/1JoaKhcXFx0/fp1ubi4KCcnR1OmTFFAQIBycnI0YsQIpaamaurU\nqSpXrpyqVq2qX375RUuWLFG9evV06tQpvffee2rYsKH69OkjSfL399dXX32lffv2KTAwUNu3b9fo\n0aM1cOBAqws0bOX999/X4sWLNX/+fHl7eysjI8NyK568iz2M2NeYMWNUvnx5mc1mnT592rKGcsyY\nMapRo4YheypfvrzlTGve15IlS1S9enXL3S2M2NfIkSMtP6sjR45o8ODBOn78uKZOnWrY/68kqXr1\n6po4caIcHBzk5eWljRs3aty4cRo0aJDq168vyZg/L0l699131bZtWz3//PNW4/f6HC9fvrzOnj2r\nOXPmqHbt2kpPT9fAgQNVuXJlffTRRzKZTPLz81NsbKzWrVun0NBQHTp0SIMHD1ZkZKSee+65YukH\n+DPjdlj3Yd68eZo2bZpSUlIUEhKi6OjoEnXGdcuWLXr++efzrbPq2rWr5Z6JEyZM0FdffaW0tDQ1\naNBAkyZNsrqZelpamoYMGWK5Rc5zzz2niRMnWoXzQ4cO6f3339d///tfVa5cWT179tTgwYOLpSdX\nV9fbrhsbMmSIhgwZYvneaH29/fbb2rJli1JTU+Xi4qLQ0FANGDDA6opno/V0O88//7xCQkKsbjhv\ntL569eql7du368KFC3Jzc1PDhg01YsQIq1+zG62nPGvXrtWYMWN09OhR+fj4KCoqyhI4jdrb5s2b\nFRERoXXr1t32DOi9PsevXbumkSNHaunSpcrMzFTz5s01adIkPfLII5Y5p0+f1qBBg7R582Y5OTmp\nc+fO+vjjj1W2bNli6Qn4MyO4AgAAwBBY4woAAABDILgCAADAEAiuAAAAMASCKwAAAAyB4AoAAABD\nILgCAADAEAiuAAAAMASCKwAAAAyB4AoAAABD+H8N2MT4659xyQAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<matplotlib.figure.Figure at 0x7fd089afa438>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "combined.barh(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how the three blue bars are almost equal – the original choice is equally likely to be any of the three available items. But the gold bar corresponding to `Car` is twice as long as the blue. \n",
    "\n",
    "The simulation confirms that the contestant is twice as likely to win if she switches."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
